
This project is pair with project auth (not security)

build image
```bash
./gradlew build && docker build -t shilong/bo-api .
```

run the container
```bash
docker container run -p 8080:8888 shilong/bo-api
```
clear docker containers
```aidl
docker rm -f $(docker ps -a -q)
```


```
$ ./gradlew build && docker image build -t shilong/bo-api . && docker push shilong/bo-api
```


```bash
java -jar build/libs/marseille-bo-api-1.0.0-SNAPSHOT-fat.jar -conf application.json
```


Create user account 

```bash
curl -X POST \
  http://localhost:8888/accounts \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 9b9b7d81-1664-406a-b4ac-9dbe07d9b702' \
  -H 'cache-control: no-cache' \
  -d '{
	"username": "me@szhao.org",
	"password": "12345"
}'
```

Request for token

```bash
curl -X POST \
  http://localhost:8888/token \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4c384803-24e3-47ce-b05c-bcdf8958b6d8' \
  -H 'cache-control: no-cache' \
  -d '{
	"username": "me@szhao.org",
	"password": "12345"
}'
```
