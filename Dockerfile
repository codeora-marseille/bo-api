FROM openjdk:8-jre-alpine

ENV VERTICLE_FILE marseille-bo-api-1.0.0-SNAPSHOT-fat.jar
ENV VERTICLE_HOME /usr/verticles

ENV CONF_JSON application-prod.json

EXPOSE 8080

COPY build/libs/$VERTICLE_FILE $VERTICLE_HOME/
COPY src/main/resources/application-prod.json $VERTICLE_HOME/

WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $VERTICLE_FILE -conf $CONF_JSON"]

