package org.szhao.api;

import io.vertx.core.*;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.szhao.api.mock.JWTAuthMockVerticle;

import java.util.Arrays;

/**
 * author: zhaoshilong
 * date: 2019-05-05
 */
@ExtendWith(VertxExtension.class)
public class TestMainVerticle {

  private static final Logger LOG = LoggerFactory.getLogger(TestMainVerticle.class);

  private static final int HTTP_PORT = 8888;
  private static final String HTTP_HOST = "localhost";



  @BeforeAll
  public static void deployVerticles(Vertx vertx, VertxTestContext testContext) {

    JsonObject testContextConfig = new JsonObject().put("http.port", HTTP_PORT);

    final int MOCK_PORT = 8889;
    final String MOCK_HOST = "localhost";

    JsonObject mockConfig = new JsonObject().put("http.port", MOCK_PORT).put("http.host", MOCK_HOST);
    testContextConfig.put("auth-service", mockConfig);

    Future<String> mainFuture = Future.future();
    vertx.deployVerticle(new MainVerticle(), new DeploymentOptions().setConfig(testContextConfig), event -> {
      if (event.succeeded()) mainFuture.complete(event.result());
      else mainFuture.fail(event.cause());
    });

    Future<String> mockFuture = Future.future();
    vertx.deployVerticle(new JWTAuthMockVerticle(), new DeploymentOptions().setConfig(testContextConfig), event -> {
      if (event.succeeded()) mockFuture.complete(event.result());
      else mockFuture.fail(event.cause());
    });


    CompositeFuture.all(Arrays.asList(mockFuture, mainFuture)).setHandler(ar -> {
      if (ar.succeeded()) testContext.completeNow();
      else testContext.failNow(ar.cause());
    });

  }

  @Test
  public void createAccount(Vertx vertx, VertxTestContext testContext) {
    JsonObject createAccountJson = new JsonObject().put("username", "szhao").put("password", "1234");

    HttpClientRequest request = vertx.createHttpClient().post(HTTP_PORT, HTTP_HOST, "/accounts", resp -> {
      resp.bodyHandler(buffer -> {
        LOG.info("Received response from " + buffer.toString());
        testContext.completeNow();
      });
    });
    request.putHeader("Content-Type", "application/json").end(createAccountJson.encodePrettily());
  }

  @AfterAll
  public static void undeployVerticles(Vertx vertx, VertxTestContext testContext) {
    vertx.close(ar -> {
      testContext.completeNow();
    });
  }

}
