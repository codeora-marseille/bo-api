package org.szhao.api;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * author: zhaoshilong
 * date: 2019-05-08
 */
@ExtendWith(VertxExtension.class)
public class TestGetVehicles {
  private static final Logger LOG = LoggerFactory.getLogger(TestGetVehicles.class);

  private static final int HTTP_PORT = 8888;
  private static final String HTTP_HOST = "localhost";

  @BeforeAll
  public static void deployVerticles(Vertx vertx, VertxTestContext testContext) {
    testContext.completeNow();
  }

  @Test
  public void testWithToken(Vertx vertx, VertxTestContext testContext) {
    testContext.completeNow();
  }

  @AfterAll
  public static void undeployVerticles(Vertx vertx, VertxTestContext testContext) {
    vertx.close(ar -> {
      testContext.completeNow();
    });
  }
}
