package org.szhao.api.mock;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.szhao.api.JWTAuthVerticle;

/**
 * author: zhaoshilong
 * date: 2019-05-04
 */
public class JWTAuthMockVerticle extends AbstractVerticle {

  private static final Logger LOG = LoggerFactory.getLogger(JWTAuthMockVerticle.class);

  @Override
  public void start(Future<Void> future) {
    Router router = Router.router(vertx);

    router.route(HttpMethod.POST, "/accounts").handler(createAccountMockHandler);
    router.route(HttpMethod.POST, "/token").handler(tokenRequestHandler);

    String host = config().getJsonObject("auth-service").getString("http.host");
    Integer port = config().getJsonObject("auth-service").getInteger("http.port");

    vertx.createHttpServer().requestHandler(router).listen(port, host, ar -> {
      if (ar.succeeded()) {
        future.complete();
      } else {
        future.fail(ar.cause());
      }
    });

  }
  private final Handler<RoutingContext> createAccountMockHandler = context -> {
    context.request().bodyHandler(buffer -> {
      // TODO: more detailed test
      context.response().end(buffer.toString());
    });
  };

  private final Handler<RoutingContext> tokenRequestHandler = context -> {
    context.request().bodyHandler(buffer -> {
      // TODO: more detailed test
      context.response().end(buffer.toString());
    });
  };
}
