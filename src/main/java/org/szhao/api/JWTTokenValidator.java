package org.szhao.api;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;

/**
 * author: zhaoshilong
 * date: 2019-05-07
 */
public class JWTTokenValidator {

  private static final Logger LOG = LoggerFactory.getLogger(JWTTokenValidator.class);

  private final AuthProvider jwtAuthProvider;

  private static final String PUBLIC_KEY =
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArx7lPpGLa3VlK+Uc+chk" +
    "OMnN+ofLLZUWjG5g4iTIjrLolW4CVb+GJxTc2A/4/Ik6UkXW9cjRDD+KgzuhS3DM\n" +
    "5rdNb3puPotm0e7bqhqehhJOW/i1j6eRngFtvcMYhveH6VgTRQp8Tv0Yv+5oPxGT\n" +
    "6fRxb7d2FgQkVvHbHGOpNvrfAiH6mFzADkA3O/FKsvJ21ANrKCC0CT3EONHZcLEK\n" +
    "vFZOxpfqJy+3797u7aG2QZ9lpqZhF8cihkDSP9n5ROR5/Aizpx6l5NmGV78igozH\n" +
    "fGDHECK2YLwSlYG7QBSM7tzNh5kjfDiE+81Sx4tkNYHI2k7C6LBXm5IBcD1nZv0g\n" +
    "6wIDAQAB";

  public JWTTokenValidator(Vertx vertx) {
    JsonObject config = new JsonObject().put("permissionClaimKeys", "authorities");

    PubSecKeyOptions pubSecKeyOptions = new PubSecKeyOptions().setAlgorithm("RS256").setPublicKey(PUBLIC_KEY);

    JWTAuthOptions options = new JWTAuthOptions(config).addPubSecKey(pubSecKeyOptions);

    jwtAuthProvider = JWTAuth.create(vertx, options);
  }

  public Future<User> validate(String token) {
    Future<User> validateFuture = Future.future();
    jwtAuthProvider.authenticate(new JsonObject().put("jwt", token).put("options", new JsonObject().put("ignoreExpiration", true)), event -> {
      if (event.succeeded()) {
        LOG.info("User authenticated: " + event.result());
        validateFuture.complete(event.result());
      }
      else {
        LOG.info("Authentication failed");
        validateFuture.fail(event.cause());
      }
    });

    return validateFuture;
  }

}
