package org.szhao.api;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

  private static final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    // ========= deploy verticles ==============

    vertx.deployVerticle(new JWTAuthVerticle(), new DeploymentOptions().setConfig(config()), LOG::info);

    // ========= config router handlers ===========
    final Router router = Router.router(vertx);

    router.route(HttpMethod.POST, "/token").handler(authHandler);
    router.route(HttpMethod.POST, "/accounts").handler(registerHandler);
    router.route(HttpMethod.GET, "/vehicles").handler(this::authenticationHandle).handler(getVehiclesHandler);

    Future<HttpServer> serverFuture = Future.future();

    vertx.createHttpServer().requestHandler(router).listen(config().getInteger("http.port"), ar -> {
      if (ar.succeeded()) {
        LOG.info("OK server started");
        serverFuture.complete(ar.result());
      }
      else {
        serverFuture.fail(ar.cause());
      }
    });

    CompositeFuture.all(Arrays.asList(serverFuture)).setHandler(ar -> {
      if (ar.succeeded()) {
        LOG.info("Application started on PORT " + config().getInteger("http.port"));
        startFuture.complete();
      }
      else {
        LOG.info("Application start failed " + ar.cause());
        startFuture.fail(ar.cause());
      }
    });
  }

  // ==================== Handlers =========================

  // simple relay handler
  private Handler<RoutingContext> authHandler = context -> {
    context.request().bodyHandler(buffer -> {
      LOG.info("Request POST /token BODY " + buffer.toString());
      vertx.eventBus().send("auth:post:token", buffer.toString(), ar -> {
        Object body = ar.result().body();
        LOG.info("POST /token response " + body);
        context.response().setStatusCode(200)
          .putHeader("Authorization", body == null? "": body.toString()).end();
      });
    });
  };

  // simple relay handler
  private Handler<RoutingContext> registerHandler = context -> {
    context.request().bodyHandler(buffer -> {
      LOG.info("Request POST /accounts BODY " + buffer.toString());
      vertx.eventBus().send("auth:post:accounts", buffer.toString(), ar -> {
        Object body = ar.result().body();
        LOG.info("create account response " + body);
        context.response().setStatusCode(200).end(body == null? "": body.toString());
      });
    });
  };

  private void authenticationHandle(RoutingContext context) {
    LOG.info(String.format("Authenticating request %s %s", context.request().method(), context.request().path()));
    JWTTokenValidator validator = new JWTTokenValidator(vertx);
    String token = context.request().getHeader("Authorization").replace("Bearer ", "");
    LOG.info("Authenticating token: " + token);

    validator.validate(token).setHandler(ar -> {
      if (ar.succeeded()) {
        context.setUser(ar.result());
        context.next();
      } else {
        context.response().setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(ar.cause().toString());
      }
    });
  }

  private Handler<RoutingContext> getVehiclesHandler = context -> {
    LOG.info("Request GET /vehicles query: " + context.request().query());
    // TODO: call real vehicles service
    context.response().setStatusCode(HttpResponseStatus.OK.code()).end();
  };
}
