package org.szhao.api;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class JWTAuthVerticle extends AbstractVerticle {
  private static final Logger LOG = LoggerFactory.getLogger(JWTAuthVerticle.class);

  public JWTAuthVerticle() {
    super();
  }

  @Override
  public void start() {
    LOG.info("Starting jwt verticle with config: " + config().encodePrettily());
    Integer port = config().getJsonObject("auth-service").getInteger("http.port");
    String host = config().getJsonObject("auth-service").getString("http.host");

    vertx.eventBus().consumer("auth:post:token").handler(msg -> postForToken(port, host, "/token", msg));
    vertx.eventBus().consumer("auth:post:accounts").handler(msg -> createAccount(port, host, "/accounts", msg));

  }

  private void createAccount(int port, String host, String path, Message<Object> msg) {
    HttpClientRequest post = vertx.createHttpClient().post(port, host, path, resp -> {

      resp.bodyHandler(buffer -> {
        msg.reply(buffer.toString());
      });

    });
    post.setTimeout(1000).exceptionHandler(e -> {
      msg.reply(e.getMessage());
    }).putHeader("Content-Type", "application/json").end(msg.body().toString());
  }

  private void postForToken(int port, String host, String path, Message<Object> msg) {

    HttpClientRequest post = vertx.createHttpClient().post(port, host, path, resp -> {
      LOG.info("received response from auth service");
      String token = resp.getHeader("Authorization");
      msg.reply(token);
    });
    post.setTimeout(1000).exceptionHandler(e -> {
      msg.reply(e.getMessage());
    }).putHeader("Content-Type", "application/json").end(msg.body().toString());

  }
}
